# Equal Experts Calculator

[VIEW DEMO](https://equal-experts-calculator.vercel.app/)

## About This Project:

**Version Number:** 72cf4fe47f85c39779267d0ecee07655a354e623

Please note that this project was not intended to be used in a
production environment. It was simply built as a lightweight
personal project and may contain some bugs.

## Basic Instructions:

- **C** - This button clears the current input
- **MS** - Memory Store - This button saves the current input to memory.
Entire equations can be stored to memory and recalled at a later
stage. To clear the memory, simply hit the C button to clear input
and then hit the MS button
- **MR** - Memory Recall - This button will recall the current value in
the memory. For example, should the current memory hold the value
'345', the user can start an equation by typing '500', then '-',
and then MR to form the equation of '500 - 345'
- To use the keyboard as input method, simply click on the "Input
Screen" of the calculator and start typing. Validation is done to
ensure invalid characthers aren't allowed. Use the **'Enter'** key to perform current calculation, use the **'Backspace'** key
to make corrections to the equation or use the **'Del'** key to
clear the input entirely

## The Goal:

The idea was to build a functional basic calculator with a
minimalistic yet modern design. The code structure was kept
simplistic and the packages included kept to a relative minimum to
ensure a small build as well as fast loading times, but at the
same time cover most of the basic functionality

## Technology Used:

The IDE Used was VS Code with GitBash installed for Windows as a
CLI terminal

Frameworks and Libraries include:

- NodeJS
- ReactJS
- ReactRouter
- Mathjs
- Material-UI

## Time Management:

### Overview

Overall time was spent fairly evenly between Development and Unit
/ Integration testing as the project moved along. Testing was done
as each Dev feature was completed with minor regression testing
due to time constraints. Once the project was in a "Release
Candidate" state some minor regression testing was done.

Initially a little time was spent just researching whether it
would be the better option to try and build the "maths" functions
myself, or if a lightweight library would suffice. I quickly
decided to use Mathjs and rather spend more time polishing any
fringe case features manually, giving me a little more time to
include one or two extra features as well like adding a "memory"
function that was really easy to impliment, but adds a lot of
value to the user.

Not to many extras was added, again with time contraints kept in
mind, as I felt making sure that all functionalities work
currectly was more important with basic Responsive Design also
tested, but not as a priority.

### Slightly Time Consuming But Necessary

One of the features added that took a little bit of extra time,
but I felt was priority was Validation of the "Equation" string.
If an incorrect string is passed to Mathjs then the program would
crash. To avoid any crashes I added very basic validation to
ensure a couple of rules aren't broken. The user will be notified
that an invalid equation has been entered if:

- The equation starts or ends with an operator such as '+', '-',
'*', '/' etc.
- The equation includes any two of the above mentioned operators
following each other
- If divided by 0, Mathjs returns 'Infinity' - Converted
this to read "Cannot Devide By Zero" as a response to the user

### Unnecessary Time Spent

Admittedly some extra time was spend to include this about page as
an extra page on the project via "React-Router-Dom". This did not
take that much time as I am very fimiliar with this feature
writing other applications that use this to determine
Authenticated Routes, but in hindsight this was probably not
necessary and could have been added on a single page

### Known Issues

Trying to compromise for time, there are one or two known issues
that I did not address:

- After calculating and equation and then clicking on a new
"integer" (non operator), the integer value just gets added to the
initial result instead of starting a new equation. Using the
keyboard to add a new integer appends the integer to the initial
result
- When stress testing the calculator I got it to crash once with
current build. I'm not entirely sure what the steps would be to
reproduce this, but if I had a little more time I would
investigate what caused the crash. Initial debugging done, but
could not find the issue straight away. As this is a fringe case,
I decided not to spend to much time on this, but should this
project have had to go to a production environment this would be
of higher priority

### If I Had More Time

If I had a little more time these are some features I might have
added and code I might have cleaned up a little more:

- Add a Percentage functionality to the calculator reading the
last "integer" value from the equation string and transforming it
into a percentage multiplyer
- Used Material UI Elements to better style the "About" page,
although the basic layout was sufficient and results in less
packages included making for a smaller project size
- Moved the validation code for the calulator to a separate
utility and import in from there, then pass the equation string to
it to validate. This would declutter the Calculator.js file and
allow other future components to make use of the same validation
functions
- Maybe if the project was to grow in size beyond just a
simple calculator I would have implimented Redux as a global store
so that the values of certain states could be used globally.
