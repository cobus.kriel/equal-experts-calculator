import React, { Component } from "react";
import Link from "react-router-dom/Link";

// Material UI Imports
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Button from "@material-ui/core/Button";

import Logo from "../images/logoFull.svg";

export class BasicNavbar extends Component {
  render() {
    return (
      <div>
        <AppBar>
          <Toolbar>
            <img className="mainLogo" src={Logo} alt="Logo" />
            <Button color="inherit" component={Link} to="/">
              Home
            </Button>
            <Button color="inherit" component={Link} to="/about">
              About
            </Button>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

export default BasicNavbar;
