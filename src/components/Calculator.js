import React, { Component } from "react";
import Link from "react-router-dom/Link";
import * as math from "mathjs";

// Material-UI Imports
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import TextField from "@material-ui/core/TextField";
import Tooltip from "@material-ui/core/Tooltip";

// Image Imports
import logoEqual from "../images/logo.png";
import logoFull from "../images/logoFull.svg";

const allowedValues = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
const allowedFunctions = ["/", "*", "-", "+", "."];

export class Calculator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      input: "",
      memory: "",
      error: "",
    };
  }

  addToInput = (val) => {
    this.setState({ input: this.state.input + val });
  };

  handleEqual = () => {
    // Validation of Equation
    let inputString = this.state.input;
    if (
      allowedValues.includes(inputString.charAt(0)) &&
      allowedValues.includes(inputString.slice(-1))
    ) {
      for (var i = 0; i < inputString.length - 1; ) {
        let charCheck1 = inputString.charAt(i);
        let charCheck2 = inputString.charAt(++i);
        if (
          allowedFunctions.includes(charCheck1) &&
          allowedFunctions.includes(charCheck2)
        ) {
          return this.setState({ error: "Invalid Equation" });
        }
      }
      this.setState({ input: math.evaluate(this.state.input), error: "" });
    } else {
      return this.setState({ error: "Invalid Equation" });
    }
  };

  handleClear = () => {
    this.setState({ input: "", error: "" });
  };

  handleInput = (value) => () => {
    if (value === "square") {
      const total = math.evaluate(this.state.input);
      this.setState({
        input: total * total,
      });
    } else if (value === "root") {
      const total = math.evaluate(this.state.input);
      this.setState({
        input: Math.sqrt(total),
      });
    } else
      this.setState({
        input: this.state.input + value,
      });
  };

  handleMemStore = () => {
    this.setState({ memory: this.state.input });
    this.setState({ input: "" });
  };

  handleMemRecall = () => {
    this.setState({ input: this.state.input + this.state.memory });
  };

  handleKeyPress = (event) => {
    let keyValue = event.key;

    if (
      allowedValues.includes(keyValue) ||
      allowedFunctions.includes(keyValue)
    ) {
      this.setState({
        input: this.state.input + keyValue,
      });
    }

    if (keyValue === ",") {
      this.setState({
        input: this.state.input + ".",
      });
    }

    if (keyValue === "Enter" || keyValue === "=") {
      this.handleEqual();
    }

    if (keyValue === "Backspace") {
      this.setState({
        input: this.state.input.slice(0, -1),
      });
    }

    if (keyValue === "Delete") {
      this.handleClear();
    }
  };

  render() {
    let memUseState = this.state.memory === "" ? "MS" : "MS*";
    return (
      <div>
        <Card elevation="3">
          <CardMedia className="calcBranding" image={logoFull} />
          <CardContent>
            <TextField
              variant="outlined"
              label="Enter Calculation"
              value={
                this.state.input == "Infinity"
                  ? "Cannot Devide By Zero"
                  : this.state.input
              }
              onKeyUp={this.handleKeyPress}
              error={this.state.error}
              helperText={this.state.error}
              fullWidth
            ></TextField>
          </CardContent>
          <CardActions>
            <div className="calcActionsContainer">
              <ButtonGroup variant="contained" color="secondary" fullWidth>
                <Tooltip title="Clear" placement="top">
                  <Button onClick={this.handleClear}>C</Button>
                </Tooltip>
                <Tooltip title="Memory Store" placement="top">
                  <Button onClick={this.handleMemStore}>{memUseState}</Button>
                </Tooltip>
                <Tooltip title="Memory Recall" placement="top">
                  <Button onClick={this.handleMemRecall}>MR</Button>
                </Tooltip>
              </ButtonGroup>
              <hr />
              <ButtonGroup variant="text" color="primary" fullWidth>
                <Button onClick={this.handleInput("square")}>x²</Button>
                <Button onClick={this.handleInput("root")}>√</Button>
                <Button onClick={this.handleInput("(")}>(</Button>
                <Button onClick={this.handleInput(")")}>)</Button>
              </ButtonGroup>
              <hr />
              <ButtonGroup variant="text" color="primary" fullWidth>
                <Button onClick={this.handleInput(7)}>7</Button>
                <Button onClick={this.handleInput(8)}>8</Button>
                <Button onClick={this.handleInput(9)}>9</Button>
                <Button onClick={this.handleInput("*")}>x</Button>
              </ButtonGroup>
              <hr />
              <ButtonGroup variant="text" color="primary" fullWidth>
                <Button onClick={this.handleInput(4)}>4</Button>
                <Button onClick={this.handleInput(5)}>5</Button>
                <Button onClick={this.handleInput(6)}>6</Button>
                <Button onClick={this.handleInput("/")}>/</Button>
              </ButtonGroup>
              <hr />
              <ButtonGroup variant="text" color="primary" fullWidth>
                <Button onClick={this.handleInput(1)}>1</Button>
                <Button onClick={this.handleInput(2)}>2</Button>
                <Button onClick={this.handleInput(3)}>3</Button>
                <Button onClick={this.handleInput("+")}>+</Button>
              </ButtonGroup>
              <hr />
              <ButtonGroup variant="text" color="primary" fullWidth>
                <Button component={Link} to="/about">
                  ?
                </Button>
                <Button onClick={this.handleInput(0)}>0</Button>
                <Button onClick={this.handleInput(".")}>.</Button>
                <Button onClick={this.handleInput("-")}>-</Button>
              </ButtonGroup>
              <hr />
              <Tooltip title="Let's Calculate!" placement="top">
                <Button
                  onClick={this.handleEqual}
                  variant="contained"
                  color="primary"
                  fullWidth
                >
                  <img className="calcEqualLogo" src={logoEqual} alt="=" />
                </Button>
              </Tooltip>
            </div>
          </CardActions>
        </Card>
      </div>
    );
  }
}

export default Calculator;
