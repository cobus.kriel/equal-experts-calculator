import React, { Component } from "react";

// Mui Imports
import Paper from "@material-ui/core/Paper";
import Typogrophy from "@material-ui/core/Typography";

export class about extends Component {
  render() {
    return (
      <div>
        <Paper className="paddedPaper">
          <Typogrophy variant="h5" color="primary">
            About This Project:
          </Typogrophy>
          <hr />
          <Typogrophy variant="caption">
            <p>
              <b>Version Number:</b> 72cf4fe47f85c39779267d0ecee07655a354e623
              <br />
              <br />
              Please note that this project was not intended to be used in a
              production environment. It was simply built as a lightweight
              personal project and may contain some bugs.
            </p>
          </Typogrophy>

          <Typogrophy variant="h5" color="primary">
            Basic Instructions:
          </Typogrophy>
          <hr />
          <Typogrophy variant="caption">
            <p>
              C - This button clears the current input
              <br />
              <br />
              MS - Memory Store - This button saves the current input to memory.
              Entire equations can be stored to memory and recalled at a later
              stage. To clear the memory, simply hit the C button to clear input
              and then hit the MS button
              <br />
              <br />
              MR - Memory Recall - This button will recall the current value in
              the memory. For example, should the current memory hold the value
              '345', the user can start an equation by typing '500', then '-',
              and then MR to form the equation of '500 - 345'
              <br />
              <br />
              To use the keyboard as input method, simply click on the "Input
              Screen" of the calculator and start typing. Validation is done to
              ensure invalid characthers aren't allowed. Use the <b>
                'Enter'
              </b>{" "}
              key to perform current calculation, use the <b>'Backspace'</b> key
              to make corrections to the equation or use the <b>'Del'</b> key to
              clear the input entirely.
            </p>
          </Typogrophy>

          <Typogrophy variant="h5" color="primary">
            The Goal:
          </Typogrophy>
          <hr />
          <Typogrophy variant="caption">
            <p>
              The idea was to build a functional basic calculator with a
              minimalistic yet modern design. The code structure was kept
              simplistic and the packages included kept to a relative minimum to
              ensure a small build as well as fast loading times, but at the
              same time cover most of the basic functionality
            </p>
          </Typogrophy>

          <Typogrophy variant="h5" color="primary">
            Technology Used:
          </Typogrophy>
          <hr />
          <Typogrophy variant="caption">
            <p>
              The IDE Used was VS Code with GitBash installed for Windows as a
              CLI terminal.
              <br />
              <br />
              Frameworks and Libraries include:
              <br />
              - NodeJS
              <br />
              - ReactJS
              <br />
              - ReactRouter
              <br />
              - Mathjs
              <br />- Material-UI
            </p>
          </Typogrophy>

          <Typogrophy variant="h5" color="primary">
            Time Management:
          </Typogrophy>
          <hr />
          <Typogrophy variant="body1">
            <u>Overview</u>
          </Typogrophy>
          <Typogrophy variant="caption">
            <p>
              Overall time was spent fairly evenly between Development and Unit
              / Integration testing as the project moved along. Testing was done
              as each Dev feature was completed with minor regression testing
              due to time constraints. Once the project was in a "Release
              Candidate" state some minor regression testing was done.
              <br />
              <br />
              Initially a little time was spent just researching whether it
              would be the better option to try and build the "maths" functions
              myself, or if a lightweight library would suffice. I quickly
              decided to use Mathjs and rather spend more time polishing any
              fringe case features manually, giving me a little more time to
              include one or two extra features as well like adding a "memory"
              function that was really easy to impliment, but adds a lot of
              value to the user.
              <br />
              <br />
              Not to many extras was added, again with time contraints kept in
              mind, as I felt making sure that all functionalities work
              currectly was more important with basic Responsive Design also
              tested, but not as a priority.
            </p>
          </Typogrophy>
          <Typogrophy variant="body1">
            <u>Slightly Time Consuming But Necessary</u>
          </Typogrophy>
          <Typogrophy variant="caption">
            <p>
              One of the features added that took a little bit of extra time,
              but I felt was priority was Validation of the "Equation" string.
              If an incorrect string is passed to Mathjs then the program would
              crash. To avoid any crashes I added very basic validation to
              ensure a couple of rules aren't broken. The user will be notified
              that an invalid equation has been entered if:
              <br />
              <br />
              - The equation starts or ends with an operator such as '+', '-',
              '*', '/' etc.
              <br />
              - The equation includes any two of the above mentioned operators
              following each other
              <br />- If divided by 0, Mathjs returns 'Infinity' - Converted
              this to read "Cannot Devide By Zero" as a response to the user
            </p>
          </Typogrophy>
          <Typogrophy variant="body1">
            <u>Unnecessary Time Spent</u>
          </Typogrophy>
          <Typogrophy variant="caption">
            <p>
              Admittedly some extra time was spend to include this about page as
              an extra page on the project via "React-Router-Dom". This did not
              take that much time as I am very fimiliar with this feature
              writing other applications that use this to determine
              Authenticated Routes, but in hindsight this was probably not
              necessary and could have been added on a single page
            </p>
          </Typogrophy>
          <Typogrophy variant="body1">
            <u>Known Issues</u>
          </Typogrophy>
          <Typogrophy variant="caption">
            <p>
              Trying to compromise for time, there are one or two known issues
              that I did not address:
              <br />
              <br />
              - After calculating and equation and then clicking on a new
              "integer" (non operator), the integer value just gets added to the
              initial result instead of starting a new equation. Using the
              keyboard to add a new integer appends the integer to the initial
              result
              <br />
              - When stress testing the calculator I got it to crash once with
              current build. I'm not entirely sure what the steps would be to
              reproduce this, but if I had a little more time I would
              investigate what caused the crash. Initial debugging done, but
              could not find the issue straight away. As this is a fringe case,
              I decided not to spend to much time on this, but should this
              project have had to go to a production environment this would be
              of higher priority
              <br />
            </p>
          </Typogrophy>
          <Typogrophy variant="body1">
            <u>If I Had More Time</u>
          </Typogrophy>
          <Typogrophy variant="caption">
            <p>
              If I had a little more time these are some features I might have
              added and code I might have cleaned up a little more:
              <br />
              <br />
              - Add a Percentage functionality to the calculator reading the
              last "integer" value from the equation string and transforming it
              into a percentage multiplyer
              <br />
              - Used Material UI Elements to better style the "About" page,
              although the basic layout was sufficient and results in less
              packages included making for a smaller project size
              <br />
              - Moved the validation code for the calulator to a separate
              utility and import in from there, then pass the equation string to
              it to validate. This would declutter the Calculator.js file and
              allow other future components to make use of the same validation
              functions
              <br />- Maybe if the project was to grow in size beyond just a
              simple calculator I would have implimented Redux as a global store
              so that the values of certain states could be used globally.
            </p>
          </Typogrophy>
        </Paper>
      </div>
    );
  }
}

export default about;
