import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

// Css Imports
import "./App.css";

// Material-UI Imports
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";

// Component Imports
import BasicNavbar from "./components/BasicNavbar";

// Import Pages
import pageHome from "./pages/home";
import pageAbout from "./pages/about";
import { MuiThemeProvider } from "@material-ui/core";

const eeTheme = createMuiTheme({
  palette: {
    primary: {
      light: "#38a4d9",
      main: "#1482b8",
      dark: "#196b98",
      contrastText: "#fff",
    },
    secondary: {
      light: "#ffe552",
      main: "#fed800",
      dark: "#dfbe00",
      contrastText: "#333",
    },
  },
});

export class App extends Component {
  render() {
    return (
      <MuiThemeProvider theme={eeTheme}>
        <div className="App">
          <Router>
            <BasicNavbar />
            <div className="container">
              <div className="containerContent">
                <Switch>
                  <Route exact path="/" component={pageHome} />
                  <Route exact path="/about" component={pageAbout} />
                </Switch>
              </div>
            </div>
          </Router>
        </div>
      </MuiThemeProvider>
    );
  }
}

export default App;
